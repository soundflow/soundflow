#include "soundflow.h"
#include <Arduino.h>
#include <SoftwareSerial.h>
#include <DFMiniMp3.h>
#include <stdint.h>

#define ENABLE_VOLUME true
#define DEFAULT_VOLUME 15

// Init player
SoftwareSerial playerSerial(26, 25);
DFMiniMp3<SoftwareSerial, Mp3Notify> mp3(playerSerial);

int buttons[12] = {2,3,4,5,6,7,8,9,10,11,12};

void setup() {
    Serial.begin(115200);
    mp3.begin();
    mp3.setVolume(DEFAULT_VOLUME);
    for (int i = 0; i < 13; i++) { pinMode(buttons[i], INPUT_PULLUP); }
    Serial.println("Initialized");
}


unsigned long btnMillisPeriod = 7;
unsigned long btnMillisStart = 0;
unsigned long btnMillisLast;

void loop() {
    mp3.loop();

    if (ENABLE_VOLUME) {
      int potVal = map(analogRead(23), 0, 1023, 0, 30);
      Serial.print("Setting volume to ");
      Serial.println(potVal);
      mp3.setVolume(potVal);
    }
    
    for (int i = 0; i < 13; i++) {
      btnMillisLast = millis();
      if(btnMillisLast - btnMillisStart >= btnMillisPeriod) {
        for(uint8_t index=0; index < 13; index++) {
            if(digitalRead(buttons[index])) {
              mp3.playMp3FolderTrack(index);
            }
        }
        btnMillisStart = btnMillisLast;
    }

    }
}
